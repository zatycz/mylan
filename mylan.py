import xml.etree.ElementTree as etree
import os
import getpass
import re
import xlsxwriter
import glob
from datetime import datetime


# vycucnutí potřebých dat ze všech xml souborů v cwd a zápis do souboru ooo
ooo = open("ooo", "w")

for filename in glob.glob('*.xml'):
    tree = etree.parse(filename)
    root = tree.getroot()
    order_id = root.attrib['order_id']
    for child in root:
        if (child.tag == 'items'):
            for leaf in child:
                qty = int(float(leaf.attrib['qty']))
                code_apa = leaf.attrib['code_apa']
                ooo.write(order_id + "#" + str(qty) + "#" + code_apa + "\n")
ooo.close()


# zkopíruje soubor ooo na apl14 do složky lph, poté čeká na provedení fglgo a po stisknutí enteru si z apl14 zkopíruje /tmp/obj_mylan.txt
print("Apl14\n Login: ", end="")
login = input()
password = getpass.getpass()
os.system('sshpass -p' + password + ' scp ooo ' +
          login + '@10.158.168.5:/home2/local/ph/ooo')
#os.system('sshpass -p' + password + ' ssh zatopek@10.158.168.5 fglgo obj_mylan3')
print("Na apl14 v /home2/local/ph spusť ' fglgo obj_mylan3 ' a po dokonceni stiskni ENTER")
input()
os.system('sshpass -p' + password + ' scp ' + login +
          '@10.158.168.5:/tmp/obj_mylan.txt obj_mylan.txt')


# generování xlsx souboru
workbook = xlsxwriter.Workbook('Mylan.xlsx')

# vsechny tucne bunky budou zarovnany na stred
bold = workbook.add_format({'bold': True})
bold.set_align('center')

# nastaveni formatu data
date_format = workbook.add_format({'num_format': 'dd.mm.yyyy'})

# nastaveni sloupce s procenz
format_pct = workbook.add_format()
format_pct.set_num_format("0.00%")

center = workbook.add_format()
center.set_align('center')

# nastaveni sirky sloupcu
worksheet = workbook.add_worksheet()
worksheet.set_column(0, 2, 15)
worksheet.set_column(3, 3, 50)
worksheet.set_column(4, 6, 10)

# zaciname na 0,0
row, col = 0, 0

# prvni radek

worksheet.write(0, 0, "datum obj.", bold)
worksheet.write(0, 1, "apakod", bold)
worksheet.write(0, 2, "", bold)
worksheet.write(0, 3, "nazev", bold)
worksheet.write(0, 4, "objednano", bold)
worksheet.write(0, 5, "pokryto", bold)
worksheet.write(0, 6, "pokryto %", bold)

# generovani tabulky
# otevreme si obj_mylan.txt pro cteni, kazdy radek rozsekneme na # a data zapiseme do bunek
file = open("obj_mylan.txt", mode='r', encoding='cp1250')
for line in file.readlines():
    datum, apakod, neco, nazev, objednano, pokryto, nic = line.split("#")
    pokryto_pct = float(pokryto) / float(objednano)
    row += 1
    datum = datetime.strptime(datum, "%Y%m%d")
    worksheet.write_datetime(row, col, datum, date_format)
    worksheet.write(row, col + 1, apakod, center)
    worksheet.write(row, col + 2, neco)
    worksheet.write(row, col + 3, nazev)
    worksheet.write(row, col + 4, int(float(objednano)))
    worksheet.write(row, col + 5, int(float(pokryto)))
    worksheet.write(row, col + 6, "=F" + str(row+1) +
                    "/E" + str(row+1), format_pct)

workbook.close()
file.close()
